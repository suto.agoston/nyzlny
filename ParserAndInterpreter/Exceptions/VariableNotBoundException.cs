using System;

namespace ParserAndInterpreter
{
    public class VariableNotBoundException : Exception
    {
        public VariableNotBoundException()
        {    
        }

        public VariableNotBoundException(string message)
            : base(message)
        {
        }

        public VariableNotBoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}