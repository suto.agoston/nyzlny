using System;

namespace ParserAndInterpreter
{
    public class TypeError : Exception
    {
        public TypeError()
        {    
        }

        public TypeError(string message)
            : base(message)
        {
        }

        public TypeError(string message, Exception inner)
            : base(message, inner)
        {
        }

        public TypeError(int i, string command, ValueType type)
            : base("Paramter " + i + " of command " + command + " must be of type " + type)
        {
        }
    }
}