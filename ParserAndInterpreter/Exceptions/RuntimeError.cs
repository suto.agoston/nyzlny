using System;

namespace ParserAndInterpreter
{
    public class RuntimeError : Exception
    {
        public RuntimeError()
        {    
        }

        public RuntimeError(string message)
            : base(message)
        {
        }

        public RuntimeError(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}