using System.Collections.Generic;
using System.Text;

namespace ParserAndInterpreter
{
    public class XMLNode
    {
        public string NodeType { get; }
        public string Text { get; }
        private Dictionary<string, string> _attributes;
        private List<XMLNode> _nodes;

        public XMLNode(string nodeType)
        {
            NodeType = nodeType;
        }

        public XMLNode(string nodeType, string text)
        {
            NodeType = nodeType;
            Text = text;
        }

        public void AddAttribute(string key, string value)
        {
            if (_attributes == null)
            {
                _attributes = new Dictionary<string, string>();
            }

            _attributes[key] = value;
        }

        public void AddAttribute(string key, double value)
        {
            if (_attributes == null)
            {
                _attributes = new Dictionary<string, string>();
            }

            _attributes[key] = value + "";
        }

        public string GetAttribute(string key)
        {
            string val = null;
            _attributes?.TryGetValue(key, out val);
            return val;
        }

        public void AddNode(XMLNode node)
        {
            if (Text != null)
            {
                return;
            }

            if (_nodes == null)
            {
                _nodes = new List<XMLNode>();
            }

            _nodes.Add(node);
        }

        public XMLNode[] GetNodes()
        {
            return _nodes.ToArray();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<");
            sb.Append(NodeType);

            if (_attributes != null)
            {
                foreach (var attribute in _attributes)
                {
                    sb.Append(" ");
                    sb.Append(attribute.Key);
                    sb.Append("=\"");
                    sb.Append(attribute.Value);
                    sb.Append("\"");
                }
            }

            sb.Append(">");

            sb.Append(Text);

            if (_nodes != null)
            {
                foreach (var node in _nodes)
                {
                    sb.Append(node);
                }

            }

            sb.Append("</");
            sb.Append(NodeType);
            sb.Append(">");

            return sb.ToString();
        }
    }
}