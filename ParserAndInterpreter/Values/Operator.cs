using System.Collections.Generic;

namespace ParserAndInterpreter
{
    public enum Operator
    {
        PLUS,
        MINUS,
        MULTIPLY,
        DIVIDE,
        POWER,
        
        EQUAL,
        NOTEQUAL,
        
        GREATER,
        GREATEREQ,
        LESS,
        LESSEQ,
        
        NOT,
        AND,
        OR,
        
        OPENING,
        CLOSING,
        
        MARK,
    }

    public static class OperatorUtil
    {
        public static readonly Dictionary<string, Operator> Operators = new Dictionary<string, Operator>();

        static OperatorUtil()
        {
            Operators.Add("+", Operator.PLUS);
            Operators.Add("-", Operator.MINUS);
            Operators.Add("*", Operator.MULTIPLY);
            Operators.Add("/", Operator.DIVIDE);
            Operators.Add("^", Operator.POWER);
            
            Operators.Add("==", Operator.EQUAL);
            Operators.Add("!=", Operator.NOTEQUAL);
            
            Operators.Add(">", Operator.GREATER);
            Operators.Add(">=", Operator.GREATEREQ);
            Operators.Add("<", Operator.LESS);
            Operators.Add("<=", Operator.LESSEQ);
            
            Operators.Add("!", Operator.NOT);
            Operators.Add("&&", Operator.AND);
            Operators.Add("||", Operator.OR);
            
            Operators.Add("(", Operator.OPENING);
            Operators.Add(")", Operator.CLOSING);
        }

        public static bool IsOperatorChar(char c)
        {
            return "+-*/^&|!()<>=".Contains(c);
        }
        
        public static int Priority(this Operator op)
        {
            switch (op)
            {
                
                case Operator.OR:
                    return 1;
                case Operator.AND:
                    return 2;
                case Operator.NOTEQUAL:
                case Operator.EQUAL:
                case Operator.GREATER:
                case Operator.GREATEREQ:
                case Operator.LESS:
                case Operator.LESSEQ:
                    return 3;
                case Operator.PLUS:
                    return 4;
                case Operator.MINUS:
                    return 5;
                case Operator.MULTIPLY:
                    return 6;
                case Operator.DIVIDE:
                    return 7;
                case Operator.POWER:
                    return 8;
                case Operator.NOT:
                    return 9;
                default:
                    return 0;
            }
        }
    }
}