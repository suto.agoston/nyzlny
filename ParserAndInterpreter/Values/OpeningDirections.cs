using System;

namespace ParserAndInterpreter
{
    [Flags]
    public enum OpeningDirections
    {
        Right = 0x01,
        Left = 0x02,
        Top = 0x04,
        Bottom = 0x08,
    }
}