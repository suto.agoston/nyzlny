using System;
using System.Collections.Generic;

namespace ParserAndInterpreter
{
    [Flags]
    public enum ValueType
    {
        INTEGER = 0x01,
        DOUBLE = 0x02,
        OPENING_DIRECTION = 0x04,
        NUMERIC = INTEGER | DOUBLE | OPENING_DIRECTION,
        BOOLEAN = 0x08,
        STRING = 0x10,
        XMLNODE = 0x20,
    }

    public abstract class SubstitutionValue : IEvaluable
    {
        public abstract ValueType ValueType();

        public abstract string GetAsString();

        public IEvaluable Evaluate(Dictionary<string, IEvaluable> variableBindings)
        {
            return this;
        }

        public static SubstitutionValue CreateValue(int val)
        {
            return new IntegerValue(val);
        }

        public static SubstitutionValue CreateValue(double val)
        {
            return new DoubleValue(val);
        }

        public static SubstitutionValue CreateValue(OpeningDirections val)
        {
            return new OpeningDirectionValue(val);
        }

        public static SubstitutionValue CreateValue(bool val)
        {
            return new BooleanValue(val);
        }

        public static SubstitutionValue CreateValue(string val)
        {
            return new StringValue(val);
        }

        public static SubstitutionValue CreateValue(XMLNode val)
        {
            return new XMLValue(val);
        }

        public void Execute(Dictionary<string, IEvaluable> variableBindings)
        {
            throw new IllegalInstructionException();
        }

        public IEnumerable<string> GetVariableNames()
        {
            return new List<string>();
        }
    }

    public abstract class NumericValue : SubstitutionValue
    {
        public abstract double getValue();
    }

    public class IntegerValue : NumericValue
    {
        public int Val { get; }

        public IntegerValue(int val)
        {
            Val = val;
        }

        public override ValueType ValueType()
        {
            return ParserAndInterpreter.ValueType.INTEGER;
        }

        public override string GetAsString()
        {
            return Val.ToString();
        }

        public override double getValue()
        {
            return Val;
        }
    }

    public class OpeningDirectionValue : NumericValue
    {
        public OpeningDirections Val { get; }

        public OpeningDirectionValue(OpeningDirections val)
        {
            Val = val;
        }

        public override ValueType ValueType()
        {
            return ParserAndInterpreter.ValueType.OPENING_DIRECTION;
        }

        public override string GetAsString()
        {
            return Val.ToString();
        }

        public override double getValue()
        {
            return (double) Val;
        }
    }

    public class DoubleValue : NumericValue
    {
        public double Val { get; }

        public DoubleValue(double val)
        {
            Val = val;
        }

        public override ValueType ValueType()
        {
            return ParserAndInterpreter.ValueType.DOUBLE;
        }

        public override string GetAsString()
        {
            return Val.ToString();
        }

        public override double getValue()
        {
            return Val;
        }
    }

    public class BooleanValue : SubstitutionValue
    {
        public bool Val { get; }

        public BooleanValue(bool val)
        {
            Val = val;
        }

        public override ValueType ValueType()
        {
            return ParserAndInterpreter.ValueType.BOOLEAN;
        }

        public override string GetAsString()
        {
            return Val.ToString();
        }
    }

    public class StringValue : SubstitutionValue
    {
        public string Val { get; }

        public StringValue(string val)
        {
            Val = val;
        }

        public override ValueType ValueType()
        {
            return ParserAndInterpreter.ValueType.STRING;
        }

        public override string GetAsString()
        {
            return Val;
        }
    }

    public class XMLValue : SubstitutionValue
    {
        public XMLNode Val { get; }

        public XMLValue(XMLNode val)
        {
            Val = val;
        }

        public override ValueType ValueType()
        {
            return ParserAndInterpreter.ValueType.XMLNODE;
        }

        public override string GetAsString()
        {
            return Val.ToString();
        }
    }
}
