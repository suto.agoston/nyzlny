using System;
using System.Collections.Generic;
using System.Linq;

namespace ParserAndInterpreter
{
    public class OperatorNode : IEvaluable
    {
        private readonly Operator _op;
        private readonly IEvaluable _left;
        private readonly IEvaluable _right;

        public OperatorNode(Operator op, IEvaluable right, IEvaluable left)
        {
            _op = op;
            _left = left;
            _right = right;
        }

        public IEvaluable Evaluate(Dictionary<string, IEvaluable> variableBindings)
        {
            SubstitutionValue leftEval = (SubstitutionValue) _left.Evaluate(variableBindings);
            SubstitutionValue rightEval = (SubstitutionValue) _right?.Evaluate(variableBindings);

            ValueType leftType = leftEval.ValueType();
            ValueType? rightType = rightEval?.ValueType();

            int? leftI = null;
            int? rightI = null;

            double? leftD = null;
            double? rightD = null;

            string leftS = null;
            string rightS = null;

            bool? leftB = null;
            bool? rightB = null;

            if (leftType == ValueType.INTEGER)
            {
                leftI = ((IntegerValue) leftEval).Val;
            }

            if (leftType == ValueType.DOUBLE)
            {
                leftD = ((DoubleValue) leftEval).Val;
            }

            if (leftType == ValueType.STRING)
            {
                leftS = ((StringValue) leftEval).Val;
            }

            if (leftType == ValueType.BOOLEAN)
            {
                leftB = ((BooleanValue) leftEval).Val;
            }

            if (rightType == ValueType.INTEGER)
            {
                rightI = ((IntegerValue) rightEval).Val;
            }

            if (rightType == ValueType.DOUBLE)
            {
                rightD = ((DoubleValue) rightEval).Val;
            }

            if (rightType == ValueType.STRING)
            {
                rightS = ((StringValue) rightEval).Val;
            }

            if (rightType == ValueType.BOOLEAN)
            {
                rightB = ((BooleanValue) rightEval).Val;
            }

            switch (_op)
            {
                case Operator.PLUS:
                    if (leftS != null && rightS != null)
                    {
                        return SubstitutionValue.CreateValue(leftS + rightS);
                    }

                    if (leftI != null && rightI != null)
                    {
                        return SubstitutionValue.CreateValue(leftI.Value + rightI.Value);
                    }

                    if ((leftI != null || leftD != null) && (rightI != null || rightD != null))
                    {
                        return SubstitutionValue.CreateValue(
                            (leftI ?? leftD).Value + (rightI ?? rightD).Value);
                    }

                    break;

                case Operator.MINUS:
                    if (leftI != null && rightI != null)
                    {
                        return SubstitutionValue.CreateValue(leftI.Value - rightI.Value);
                    }

                    if ((leftI != null || leftD != null) && (rightI != null || rightD != null))
                    {
                        return SubstitutionValue.CreateValue(
                            (leftI ?? leftD).Value - (rightI ?? rightD).Value);
                    }

                    break;

                case Operator.MULTIPLY:
                    if (leftI != null && rightI != null)
                    {
                        return SubstitutionValue.CreateValue(leftI.Value * rightI.Value);
                    }

                    if ((leftI != null || leftD != null) && (rightI != null || rightD != null))
                    {
                        return SubstitutionValue.CreateValue(
                            (leftI ?? leftD).Value * (rightI ?? rightD).Value);
                    }

                    break;

                case Operator.DIVIDE:
                    if (leftI != null && rightI != null)
                    {
                        return SubstitutionValue.CreateValue((leftI / rightI).Value);
                    }

                    if ((leftI != null || leftD != null) && (rightI != null || rightD != null))
                    {
                        return SubstitutionValue.CreateValue(
                            (leftI ?? leftD).Value / (rightI ?? rightD).Value);
                    }

                    break;

                case Operator.POWER:
                    if ((leftI != null || leftD != null) && (rightI != null || rightD != null))
                    {
                        return SubstitutionValue.CreateValue(
                            Math.Pow((leftI ?? leftD).Value, (rightI ?? rightD).Value));
                    }

                    break;

                case Operator.LESS:
                    if ((leftI != null || leftD != null) && (rightI != null || rightD != null))
                    {
                        return SubstitutionValue.CreateValue(
                            (leftI ?? leftD).Value < (rightI ?? rightD).Value);
                    }

                    break;

                case Operator.LESSEQ:
                    if ((leftI != null || leftD != null) && (rightI != null || rightD != null))
                    {
                        return SubstitutionValue.CreateValue(
                            (leftI ?? leftD).Value <= (rightI ?? rightD).Value);
                    }

                    break;

                case Operator.GREATER:
                    if ((leftI != null || leftD != null) && (rightI != null || rightD != null))
                    {
                        return SubstitutionValue.CreateValue(
                            (leftI ?? leftD).Value > (rightI ?? rightD).Value);
                    }

                    break;

                case Operator.GREATEREQ:
                    if ((leftI != null || leftD != null) && (rightI != null || rightD != null))
                    {
                        return SubstitutionValue.CreateValue(
                            (leftI ?? leftD).Value >= (rightI ?? rightD).Value);
                    }

                    break;

                case Operator.EQUAL:
                    if (leftB != null && rightB != null)
                    {
                        return SubstitutionValue.CreateValue(leftB == rightB);
                    }

                    if ((leftI != null || leftD != null) && (rightI != null || rightD != null))
                    {
                        return SubstitutionValue.CreateValue(
                            (leftI ?? leftD).Value == (rightI ?? rightD).Value);
                    }

                    break;

                case Operator.NOTEQUAL:
                    if (leftB != null && rightB != null)
                    {
                        return SubstitutionValue.CreateValue(leftB != rightB);
                    }

                    if ((leftI != null || leftD != null) && (rightI != null || rightD != null))
                    {
                        return SubstitutionValue.CreateValue(
                            (leftI ?? leftD).Value != (rightI ?? rightD).Value);
                    }

                    break;

                case Operator.NOT:
                    if (leftB != null && rightB == null)
                    {
                        return SubstitutionValue.CreateValue(!leftB.Value);
                    }

                    break;

                case Operator.AND:
                    if (leftB != null && rightB != null)
                    {
                        return SubstitutionValue.CreateValue(leftB.Value && rightB.Value);
                    }

                    break;

                case Operator.OR:
                    if (leftB != null && rightB != null)
                    {
                        return SubstitutionValue.CreateValue(leftB.Value || rightB.Value);
                    }

                    break;
            }

            throw new TypeError("Operator " + _op + " is incompatible with types " + leftType + ", " + rightType);
        }

        public void Execute(Dictionary<string, IEvaluable> variableBindings)
        {
            throw new IllegalInstructionException();
        }

        public IEnumerable<string> GetVariableNames()
        {
            return _left.GetVariableNames()
                .Union(_right.GetVariableNames());
        }
    }
}
