using System.Collections.Generic;

namespace ParserAndInterpreter
{
    public interface IEvaluable : IInstruction
    {
        IEvaluable Evaluate(Dictionary<string, IEvaluable> variableBindings);
    }
}