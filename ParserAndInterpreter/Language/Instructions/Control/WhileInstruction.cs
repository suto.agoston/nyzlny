using System.Collections.Generic;

namespace ParserAndInterpreter
{
    public class WhileInstruction : ControlInstruction
    {   
        public override void Execute(Dictionary<string, IEvaluable> variableBindings)
        {
            while (true)
            {
                SubstitutionValue headValue = (SubstitutionValue) Head.Evaluate(variableBindings);
                
                if (headValue.ValueType() == ValueType.BOOLEAN)
                {
                    if (((BooleanValue) headValue).Val)
                    {
                        Body.Execute(variableBindings);
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    throw new TypeError("While instruction head must be of type " + ValueType.BOOLEAN 
                           + ", but was " + headValue.ValueType());
                }
            }
        }
    }
}