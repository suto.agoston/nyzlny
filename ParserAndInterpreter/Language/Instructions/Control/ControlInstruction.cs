using System.Collections.Generic;
using System.Linq;

namespace ParserAndInterpreter
{
    public abstract class ControlInstruction : IInstruction
    {
        protected IEvaluable Head;
        protected Block Body;

        public void AddHead(IEvaluable head)
        {
            if (Head == null)
            {
                Head = head;
            }
            else
            {
                throw new IllegalInstructionException();
            }
        }

        public void AddBody(Block body)
        {
            if (Body == null)
            {
                Body = body;
            }
            else
            {
                throw new IllegalInstructionException();
            }
        }


        public IEnumerable<string> GetVariableNames()
        {
            return Head.GetVariableNames()
                .Union(Body.GetVariableNames());
        }

        public abstract void Execute(Dictionary<string, IEvaluable> variableBindings);
    }
}
