using System.Collections.Generic;

namespace ParserAndInterpreter
{
    public class IfInstruction : ControlInstruction
    {
        public override void Execute(Dictionary<string, IEvaluable> variableBindings)
        {
            var headValue = (SubstitutionValue) Head.Evaluate(variableBindings);

            if (headValue.ValueType() == ValueType.BOOLEAN)
            {
                if (((BooleanValue) headValue).Val)
                {
                    Body.Execute(variableBindings);
                }
            }
            else
            {
                throw new TypeError(
                    $"If instruction head must be of type {ValueType.BOOLEAN}, but was {headValue.ValueType()}"
                );
            }
        }
    }
}
