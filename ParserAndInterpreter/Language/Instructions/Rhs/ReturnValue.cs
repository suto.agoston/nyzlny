using System;

namespace ParserAndInterpreter
{
    public class ReturnValue : Exception
    {
        public IEvaluable Val { get; }

        public ReturnValue()
        {
        }
        
        public ReturnValue(IEvaluable val)
        {
            Val = val;
        }
    }
}