using System.Collections.Generic;

namespace ParserAndInterpreter
{
    public class AssignmentInstruction : RhsInstruction
    {
        private readonly string _name;

        public AssignmentInstruction(string name)
        {
            _name = name;
        }
        
        public override void Execute(Dictionary<string, IEvaluable> variableBindings)
        {
            IEvaluable evaluated = Rhs.Evaluate(variableBindings);
            variableBindings[_name] = evaluated;
        }
    }
}