using System.Collections.Generic;
using System.Linq;

namespace ParserAndInterpreter
{
    public class Block : IInstruction
    {
        protected readonly List<IInstruction> InstructionList;

        public Block()
        {
            InstructionList = new List<IInstruction>();
        }

        public void Push(IInstruction instruction)
        {
            InstructionList.Add(instruction);
        }

        public virtual void Execute(Dictionary<string, IEvaluable> variableBindings)
        {
            foreach (var instruction in InstructionList)
            {
                instruction.Execute(variableBindings);
            }
        }

        public IEnumerable<string> GetVariableNames()
        {
            return InstructionList
                .Select(param => param.GetVariableNames())
                .Aggregate(Enumerable.Empty<string>(), (list1, list2) => list1.Union(list2));
        }
    }
}
