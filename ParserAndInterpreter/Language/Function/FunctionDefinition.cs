using System;
using System.Collections.Generic;

namespace ParserAndInterpreter
{
    public class FunctionDefinition : Block, IEvaluable
    {
        private readonly string _name;
        private readonly List<string> _parameters = new List<string>();

        public FunctionDefinition()
        {
        }
        
        public FunctionDefinition(string name)
        {
            _name = name;
        }

        public void AddParameter(string parameter)
        {
            _parameters.Add(parameter);
        }
        
        public IEvaluable Evaluate(Dictionary<string, IEvaluable> variableBindings)
        {
            foreach (var instruction in InstructionList)
            {
                try
                {
                    instruction.Execute(variableBindings);
                }
                catch (ReturnValue e)
                {
                    return e.Val;
                }
            }
            
            throw new IllegalInstructionException("No return value in function");
        }
        
        public override void Execute(Dictionary<string, IEvaluable> variableBindings)
        {
            if (_name == null)
            {
                throw new IllegalInstructionException();
            }

            variableBindings[_name] = this;
        }

        public void ExecuteBody(Dictionary<string, IEvaluable> variableBindings)
        {
            foreach (var instruction in InstructionList)
            {
                try
                {
                    instruction.Execute(variableBindings);
                }
                catch (ReturnValue)
                {
                    return;
                }
            }
        }

        public Dictionary<string, IEvaluable> SetParameters(List<IEvaluable> parameters, 
            Dictionary<string, IEvaluable> variableBindings)
        {
            var newBindings = new Dictionary<string, IEvaluable>(variableBindings);

            for (var i = 0; i < _parameters.Count; i++)
            {
                newBindings[_parameters[i]] = parameters[i].Evaluate(variableBindings);
            }

            return newBindings;
        }
    }
}