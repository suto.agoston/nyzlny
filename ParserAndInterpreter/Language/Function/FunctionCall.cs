using System.Collections.Generic;
using System.Linq;

namespace ParserAndInterpreter
{
    public class FunctionCall : IEvaluable
    {
        private readonly string _name;

        private List<IEvaluable> _parameters;

        public void AddParameter(IEvaluable parameter)
        {
            if (_parameters == null)
            {
                _parameters = new List<IEvaluable>();
            }

            _parameters.Add(parameter);
        }

        protected IEvaluable[] EvaluateParameters(Dictionary<string, IEvaluable> variableBindings)
        {
            int count = _parameters?.Count ?? 0;

            IEvaluable[] evaluatedParameters = new IEvaluable[count];

            for (var i = 0; i < count; i++)
            {
                evaluatedParameters[i] = _parameters[i].Evaluate(variableBindings);
            }

            return evaluatedParameters;
        }

        public FunctionCall(string name)
        {
            _name = name;
        }

        public IEvaluable Evaluate(Dictionary<string, IEvaluable> variableBindings)
        {
            Commands.Command command = Commands.GetCommand(_name);

            if (command != null)
            {
                return command.Evaluate(EvaluateParameters(variableBindings));
            }

            if (variableBindings.TryGetValue(_name, out var functionDefinition))
            {
                if (functionDefinition is FunctionDefinition definition)
                {
                    var newBindings = definition.SetParameters(_parameters, variableBindings);

                    return definition.Evaluate(newBindings);
                }
            }

            throw new VariableNotBoundException("Function " + _name + " not defined");
        }

        public IEnumerable<string> GetVariableNames()
        {
            return _parameters
                .Select(param => param.GetVariableNames())
                .Aggregate(Enumerable.Empty<string>(), (list1, list2) => list1.Union(list2));
        }

        public void Execute(Dictionary<string, IEvaluable> variableBindings)
        {
            Commands.Command command = Commands.GetCommand(_name);

            if (command != null)
            {
                command.Evaluate(EvaluateParameters(variableBindings));
            }
            else if (variableBindings.TryGetValue(_name, out var functionDefinition))
            {
                if (functionDefinition is FunctionDefinition definition)
                {
                    var newBindings = definition.SetParameters(_parameters, variableBindings);

                    definition.ExecuteBody(newBindings);
                }
            }
        }
    }
}
