# NyílásZáróLeíróNyelv.

A simple, interpreted language, written in C#, created to describe vector graphics (sketches of doors and windows) and calculate the value of basic expressions. 

Part of the Woody project, a software intended to help workshops keep track of orders.

CSUBB Group Project 2018

Thanks to @myklosbotond for the help.